﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KomunalniePoslugi
{
    [System.Serializable]
    public enum TransuctionType { deposit, withdraw }

    [System.Serializable]
    public class Transaction
    {
        public string BillNumberTo { get; private set; }
        public double Amount { get; private set; }
        public TransuctionType TransuctionType { get; private set; }

        public Transaction(string billNumberTo, double amount, TransuctionType transuctionType)
        {
            BillNumberTo = billNumberTo;
            Amount = amount;
            TransuctionType = transuctionType;
        }
    }

    public class Bill
    {
        const string NOT_ENOUGH_MONEY_EXCEPTION = "Not enough money";

        public string BillNumber { get; private set; }
        public double Money { get; private set; }
        public List<Transaction> TransactionHistory { get; private set; }

        public Bill(string billNumber, double money, List<Transaction> transactionHistory)
        {
            TransactionHistory = transactionHistory;
            BillNumber = billNumber;
            Money = money;
        }

        public void AddTransaction(Transaction transaction)
        {
            if (transaction.TransuctionType == TransuctionType.deposit)
            {
                Money += transaction.Amount;
            }
            else
            {
                if ((Money - transaction.Amount) < 0)
                {
                    throw new Exception(NOT_ENOUGH_MONEY_EXCEPTION);
                }
                Money -= transaction.Amount;
            }

            if (TransactionHistory == null)
            {
                TransactionHistory = new List<Transaction>();
            }

            TransactionHistory.Add(transaction);
        }
    }
}
