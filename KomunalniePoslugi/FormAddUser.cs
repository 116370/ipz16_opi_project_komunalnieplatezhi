﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KomunalniePoslugi
{
    public partial class FormAddUser : Form
    {
        private FormMain formMain;
        private FormUsers formUsers;

        public FormAddUser(FormMain formMain, FormUsers formUsers)
        {
            this.formMain = formMain;
            this.formUsers = formUsers;

            InitializeComponent();
        }

        private void tb_sum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void tb_number_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btn_add_user_Click(object sender, EventArgs e)
        {
            double money;

            try
            {
                money = Double.Parse(tb_amount.Text);
            }
            catch { money = 0; }

            User user = new User(tb_fio.Text, new Bill(tb_number.Text, money, new List<Transaction>()));

            formMain.AddUser(user);

            formUsers.FillTable();

            Close();
        }
    }
}
