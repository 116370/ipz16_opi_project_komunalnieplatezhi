﻿namespace KomunalniePoslugi
{
    partial class FormInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_number = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.table_transactions = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.l_user_not_found = new System.Windows.Forms.Label();
            this.l_fio = new System.Windows.Forms.Label();
            this.l_sum = new System.Windows.Forms.Label();
            this.col_number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.table_transactions)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_number
            // 
            this.tb_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_number.Location = new System.Drawing.Point(17, 37);
            this.tb_number.MaxLength = 5;
            this.tb_number.Name = "tb_number";
            this.tb_number.Size = new System.Drawing.Size(102, 44);
            this.tb_number.TabIndex = 1;
            this.tb_number.TextChanged += new System.EventHandler(this.tb_number_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(279, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Номер счета плательщика";
            // 
            // table_transactions
            // 
            this.table_transactions.AllowUserToAddRows = false;
            this.table_transactions.AllowUserToDeleteRows = false;
            this.table_transactions.AllowUserToResizeColumns = false;
            this.table_transactions.AllowUserToResizeRows = false;
            this.table_transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_transactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_number,
            this.col_sum,
            this.col_type});
            this.table_transactions.Location = new System.Drawing.Point(12, 213);
            this.table_transactions.Name = "table_transactions";
            this.table_transactions.Size = new System.Drawing.Size(444, 271);
            this.table_transactions.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "ФИО:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 25);
            this.label3.TabIndex = 6;
            this.label3.Text = "Сумма на счету:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(223, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "История транзакций:";
            // 
            // l_user_not_found
            // 
            this.l_user_not_found.AutoSize = true;
            this.l_user_not_found.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_user_not_found.ForeColor = System.Drawing.Color.Red;
            this.l_user_not_found.Location = new System.Drawing.Point(158, 49);
            this.l_user_not_found.Name = "l_user_not_found";
            this.l_user_not_found.Size = new System.Drawing.Size(244, 25);
            this.l_user_not_found.TabIndex = 8;
            this.l_user_not_found.Text = "Плательщик не найден";
            // 
            // l_fio
            // 
            this.l_fio.AutoSize = true;
            this.l_fio.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_fio.Location = new System.Drawing.Point(85, 99);
            this.l_fio.Name = "l_fio";
            this.l_fio.Size = new System.Drawing.Size(0, 25);
            this.l_fio.TabIndex = 9;
            // 
            // l_sum
            // 
            this.l_sum.AutoSize = true;
            this.l_sum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_sum.Location = new System.Drawing.Point(195, 136);
            this.l_sum.Name = "l_sum";
            this.l_sum.Size = new System.Drawing.Size(0, 25);
            this.l_sum.TabIndex = 10;
            // 
            // col_number
            // 
            this.col_number.HeaderText = "Номер счета";
            this.col_number.Name = "col_number";
            // 
            // col_sum
            // 
            this.col_sum.HeaderText = "Сумма";
            this.col_sum.Name = "col_sum";
            this.col_sum.Width = 150;
            // 
            // col_type
            // 
            this.col_type.HeaderText = "Тип транзакции";
            this.col_type.Name = "col_type";
            this.col_type.Width = 150;
            // 
            // FormInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 496);
            this.Controls.Add(this.l_sum);
            this.Controls.Add(this.l_fio);
            this.Controls.Add(this.l_user_not_found);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.table_transactions);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_number);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Информация о плательщике";
            ((System.ComponentModel.ISupportInitialize)(this.table_transactions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_number;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView table_transactions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label l_user_not_found;
        private System.Windows.Forms.Label l_fio;
        private System.Windows.Forms.Label l_sum;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_number;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sum;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_type;
    }
}