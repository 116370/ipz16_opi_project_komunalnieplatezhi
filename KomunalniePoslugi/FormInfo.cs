﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KomunalniePoslugi
{
    public partial class FormInfo : Form
    {
        private FormMain formMain;

        public FormInfo(FormMain formMain)
        {
            this.formMain = formMain;

            InitializeComponent();
        }

        private void tb_number_TextChanged(object sender, EventArgs e)
        {
            l_user_not_found.Visible = false;
            l_fio.Text = "";
            l_sum.Text = "";
            table_transactions.Rows.Clear();
            table_transactions.Refresh();

            try
            {
                User user = formMain.GetUser(tb_number.Text);
                l_fio.Text = user.Fio;
                l_sum.Text = user.Bill.Money.ToString();

                // Fill table
                foreach (var t in user.Bill.TransactionHistory)
                {
                    table_transactions.Rows.Add(t.BillNumberTo, t.Amount, t.TransuctionType.ToString());
                }
            }
            catch
            {
                l_user_not_found.Visible = true;
            }
        }
    }
}
