﻿namespace KomunalniePoslugi
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_user_list = new System.Windows.Forms.Button();
            this.btn_transaction = new System.Windows.Forms.Button();
            this.btn_info = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_user_list
            // 
            this.btn_user_list.Location = new System.Drawing.Point(12, 12);
            this.btn_user_list.Name = "btn_user_list";
            this.btn_user_list.Size = new System.Drawing.Size(197, 114);
            this.btn_user_list.TabIndex = 0;
            this.btn_user_list.Text = "Список плательщиков";
            this.btn_user_list.UseVisualStyleBackColor = true;
            this.btn_user_list.Click += new System.EventHandler(this.btn_user_list_Click);
            // 
            // btn_transaction
            // 
            this.btn_transaction.Location = new System.Drawing.Point(12, 132);
            this.btn_transaction.Name = "btn_transaction";
            this.btn_transaction.Size = new System.Drawing.Size(197, 114);
            this.btn_transaction.TabIndex = 1;
            this.btn_transaction.Text = "Перевести сумму";
            this.btn_transaction.UseVisualStyleBackColor = true;
            this.btn_transaction.Click += new System.EventHandler(this.btn_transaction_Click);
            // 
            // btn_info
            // 
            this.btn_info.Location = new System.Drawing.Point(12, 252);
            this.btn_info.Name = "btn_info";
            this.btn_info.Size = new System.Drawing.Size(197, 114);
            this.btn_info.TabIndex = 2;
            this.btn_info.Text = "Информация о счете";
            this.btn_info.UseVisualStyleBackColor = true;
            this.btn_info.Click += new System.EventHandler(this.btn_info_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 375);
            this.Controls.Add(this.btn_info);
            this.Controls.Add(this.btn_transaction);
            this.Controls.Add(this.btn_user_list);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Коммунальные платежи";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_user_list;
        private System.Windows.Forms.Button btn_transaction;
        private System.Windows.Forms.Button btn_info;
    }
}

