﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace KomunalniePoslugi
{
    public partial class FormMain : Form
    {
        public List<User> UserList { get { return userList; } }

        private FormUsers formUsers;
        private FormTransactions formTransactions;
        private FormInfo formInfo;

        public const string USER_NOT_FOUND_EXCEPTION = "User not found";
        private const string DB_NAME = "database.sqlite";

        private readonly List<User> userList = new List<User>();

        public FormMain()
        {
            InitializeComponent();

            LoadUsersFromDB();
        }

        private void LoadUsersFromDB()
        {
            using (var dbConnection = new SQLiteConnection("Data Source=" + DB_NAME + ";Version=3;"))
            {
                dbConnection.Open();

                using (var command = new SQLiteCommand(dbConnection))
                {
                    command.CommandText = "select * from users";

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string fio = String.Empty;
                            string number = String.Empty;
                            double money = 0;
                            List<Transaction> history = new List<Transaction>();

                            if (reader["fio"] != null && !Convert.IsDBNull(reader["fio"]))
                            {
                                fio = reader["fio"].ToString();
                            }
                            if (reader["number"] != null && !Convert.IsDBNull(reader["number"]))
                            {
                                number = reader["number"].ToString();
                            }
                            if (reader["money"] != null && !Convert.IsDBNull(reader["money"]))
                            {
                                money = Double.Parse(reader["money"].ToString());
                            }
                            if (reader["history"] != null && !Convert.IsDBNull(reader["history"]))
                            {
                                byte[] historyBytes = (byte[])reader["history"];
                                history = (List<Transaction>)DeserializeFromStream(historyBytes);
                            }

                            User user = new User(fio, new Bill(number, money, history));

                            userList.Add(user);
                        }
                    }
                }
            }
        }

        public bool AddUser(User user)
        {
            if (!userList.Contains(user))
            {
                userList.Add(user);

                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + DB_NAME + ";Version=3;");
                dbConnection.Open();

                // Create empty transaction list
                List<Transaction> list = new List<Transaction>();
                byte[] bytes = SerializeToStream(list);

                // Insert user
                SQLiteCommand command = new SQLiteCommand(dbConnection);
                command.CommandText = "insert into users (fio, number, money, history) values (@fio, @number, @money, @history)";
                command.Parameters.Add("@fio", DbType.String).Value = user.Fio;
                command.Parameters.Add("@number", DbType.String).Value = user.Bill.BillNumber;
                command.Parameters.Add("@money", DbType.String).Value = user.Bill.Money;
                command.Parameters.Add("@history", DbType.Binary, bytes.Length * 4).Value = bytes;
                command.ExecuteNonQuery();

                return true;
            }
            else
            {
                return false;
            }
        }

        public User GetUser(string billNumber)
        {
            User foundUser = userList.Find(user => user.Bill.BillNumber == billNumber);
            if (foundUser == null)
            {
                throw new Exception(USER_NOT_FOUND_EXCEPTION);
            }
            else
            {
                return foundUser;
            }

        }

        public void MakeTransaction(User userFrom, User userTo, double amount)
        {
            Transaction transactionFrom = new Transaction(userTo.Bill.BillNumber, amount, TransuctionType.withdraw);
            userFrom.Bill.AddTransaction(transactionFrom);
            UpdateHistory(userFrom.Bill);

            Transaction transactionTo = new Transaction(userFrom.Bill.BillNumber, amount, TransuctionType.deposit);
            userTo.Bill.AddTransaction(transactionTo);
            UpdateHistory(userTo.Bill);
        }

        private void UpdateHistory(Bill bill)
        {
            using (var dbConnection = new SQLiteConnection("Data Source=" + DB_NAME + ";Version=3;"))
            {
                dbConnection.Open();

                byte[] bytes = SerializeToStream(bill.TransactionHistory);

                using (var command = new SQLiteCommand(dbConnection))
                {
                    command.CommandText = "update users set history = @history, money = @money where number = @number";
                    command.Parameters.Add("@history", DbType.Binary, bytes.Length * 4).Value = bytes;
                    command.Parameters.Add("@money", DbType.String).Value = bill.Money;
                    command.Parameters.Add("@number", DbType.String).Value = bill.BillNumber;
                    command.ExecuteNonQuery();
                }
            }
        }

        #region Buttons
        private void btn_user_list_Click(object sender, EventArgs e)
        {
            formUsers = new FormUsers(this);
            formUsers.ShowDialog();
        }

        private void btn_transaction_Click(object sender, EventArgs e)
        {
            formTransactions = new FormTransactions(this);
            formTransactions.ShowDialog();
        }

        private void btn_info_Click(object sender, EventArgs e)
        {
            formInfo = new FormInfo(this);
            formInfo.ShowDialog();
        }
        #endregion

        public static byte[] SerializeToStream(object o)
        {
            MemoryStream stream = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, o);
            return stream.ToArray();
        }

        public static object DeserializeFromStream(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                object o = formatter.Deserialize(memStream);
                return o;
            }
        }
    }
}
