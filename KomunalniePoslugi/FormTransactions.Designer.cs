﻿namespace KomunalniePoslugi
{
    partial class FormTransactions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_from = new System.Windows.Forms.TextBox();
            this.tb_to = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_sum = new System.Windows.Forms.TextBox();
            this.btn_submit_transaction = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_from
            // 
            this.tb_from.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_from.Location = new System.Drawing.Point(93, 37);
            this.tb_from.MaxLength = 5;
            this.tb_from.Name = "tb_from";
            this.tb_from.Size = new System.Drawing.Size(102, 44);
            this.tb_from.TabIndex = 0;
            this.tb_from.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_from_KeyPress);
            // 
            // tb_to
            // 
            this.tb_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_to.Location = new System.Drawing.Point(93, 173);
            this.tb_to.MaxLength = 5;
            this.tb_to.Name = "tb_to";
            this.tb_to.Size = new System.Drawing.Size(102, 44);
            this.tb_to.TabIndex = 1;
            this.tb_to.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.t_to_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(279, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Номер счета плательщика";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(261, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Номер счета получателя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(425, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Сумма";
            // 
            // tb_sum
            // 
            this.tb_sum.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tb_sum.Location = new System.Drawing.Point(342, 65);
            this.tb_sum.Name = "tb_sum";
            this.tb_sum.Size = new System.Drawing.Size(270, 44);
            this.tb_sum.TabIndex = 5;
            this.tb_sum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_sum_KeyPress);
            // 
            // btn_submit_transaction
            // 
            this.btn_submit_transaction.Location = new System.Drawing.Point(342, 127);
            this.btn_submit_transaction.Name = "btn_submit_transaction";
            this.btn_submit_transaction.Size = new System.Drawing.Size(270, 67);
            this.btn_submit_transaction.TabIndex = 6;
            this.btn_submit_transaction.Text = "Перевести";
            this.btn_submit_transaction.UseVisualStyleBackColor = true;
            this.btn_submit_transaction.Click += new System.EventHandler(this.btn_submit_transaction_Click);
            // 
            // FormTransactions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 228);
            this.Controls.Add(this.btn_submit_transaction);
            this.Controls.Add(this.tb_sum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_to);
            this.Controls.Add(this.tb_from);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTransactions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Перевести сумму";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_from;
        private System.Windows.Forms.TextBox tb_to;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_sum;
        private System.Windows.Forms.Button btn_submit_transaction;
    }
}