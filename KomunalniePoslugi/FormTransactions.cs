﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KomunalniePoslugi
{
    public partial class FormTransactions : Form
    {
        private FormMain formMain; 

        public FormTransactions(FormMain formMain)
        {
            this.formMain = formMain;

            InitializeComponent();
        }

        private void tb_sum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void tb_from_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void t_to_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btn_submit_transaction_Click(object sender, EventArgs e)
        {
            double amount;

            try
            {
                amount = Double.Parse(tb_sum.Text);
                formMain.MakeTransaction(formMain.GetUser(tb_from.Text), formMain.GetUser(tb_to.Text), amount);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неверные данные", "Ошибка: " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }

            Close();
        }
    }
}
