﻿namespace KomunalniePoslugi
{
    partial class FormUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.users_table = new System.Windows.Forms.DataGridView();
            this.col_fio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.добавитьПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_add_user = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.users_table)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // users_table
            // 
            this.users_table.AllowUserToAddRows = false;
            this.users_table.AllowUserToDeleteRows = false;
            this.users_table.AllowUserToResizeColumns = false;
            this.users_table.AllowUserToResizeRows = false;
            this.users_table.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.users_table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.users_table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_fio,
            this.col_number,
            this.col_sum});
            this.users_table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.users_table.Location = new System.Drawing.Point(0, 24);
            this.users_table.Name = "users_table";
            this.users_table.ReadOnly = true;
            this.users_table.Size = new System.Drawing.Size(491, 402);
            this.users_table.TabIndex = 0;
            // 
            // col_fio
            // 
            this.col_fio.HeaderText = "ФИО";
            this.col_fio.Name = "col_fio";
            this.col_fio.Width = 150;
            // 
            // col_number
            // 
            this.col_number.HeaderText = "Номер счета";
            this.col_number.Name = "col_number";
            this.col_number.Width = 150;
            // 
            // col_sum
            // 
            this.col_sum.HeaderText = "Сумма на счету";
            this.col_sum.Name = "col_sum";
            this.col_sum.Width = 150;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьПользователяToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(491, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // добавитьПользователяToolStripMenuItem
            // 
            this.добавитьПользователяToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_add_user});
            this.добавитьПользователяToolStripMenuItem.Name = "добавитьПользователяToolStripMenuItem";
            this.добавитьПользователяToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.добавитьПользователяToolStripMenuItem.Text = "Действия";
            // 
            // menu_add_user
            // 
            this.menu_add_user.Name = "menu_add_user";
            this.menu_add_user.Size = new System.Drawing.Size(204, 22);
            this.menu_add_user.Text = "Добавить пользователя";
            this.menu_add_user.Click += new System.EventHandler(this.menu_add_user_Click);
            // 
            // FormUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 426);
            this.Controls.Add(this.users_table);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUsers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Список плательщиков";
            ((System.ComponentModel.ISupportInitialize)(this.users_table)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView users_table;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_fio;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_number;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sum;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem добавитьПользователяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu_add_user;
    }
}