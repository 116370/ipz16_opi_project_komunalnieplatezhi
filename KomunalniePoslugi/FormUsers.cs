﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KomunalniePoslugi
{
    public partial class FormUsers : Form
    {
        private FormMain formMain;
        private FormAddUser formAddUser;

        public FormUsers(FormMain formMain)
        {
            this.formMain = formMain;
            formAddUser = new FormAddUser(formMain, this);

            InitializeComponent();

            FillTable();
        }

        public void FillTable()
        {
            users_table.Rows.Clear();
            users_table.Refresh();

            formMain.UserList.ForEach(user => 
            {
                users_table.Rows.Add(user.Fio, user.Bill.BillNumber, user.Bill.Money);
            });
        }

        private void menu_add_user_Click(object sender, EventArgs e)
        {
            formAddUser.ShowDialog();
        }
    }
}
