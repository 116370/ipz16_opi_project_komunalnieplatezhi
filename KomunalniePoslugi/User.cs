﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KomunalniePoslugi
{
    public class User
    {
        public string Fio { get; private set; }
        public Bill Bill { get; private set; }

        public User(string fio, Bill bill)
        {
            Fio = fio;
            Bill = bill;
        }

        public override bool Equals(object obj)
        {
            User user = (User)obj;
            return user.Bill.BillNumber.Equals(Bill.BillNumber);
        }
    }
}
